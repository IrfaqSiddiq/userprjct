package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"user/model"

	"github.com/gin-gonic/gin"
)
type userinfo struct{
	uid int
	uname string
	ulocation string
}
func ConnectingServr(){
	r := gin.Default()
	r.GET("/usr", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "we are connected to browser",
		})
	})
	r.Run()
}
func ReadUser(){
	var c *gin.Context
	rows,err :=model.Db.Query("select * from bookuser")
	if err!=nil{
		fmt.Println("error in opening database")
		//error handling not sure
		c.JSON(200,gin.H{
			"message":"databse problem",
		})
		panic(err)
	}
	defer rows.Close()
	usrs:=make([]userinfo,0)
	for rows.Next(){
		usr:=userinfo{}
		err:=rows.Scan(&usr.uid,&usr.uname,&usr.ulocation)
		if err!=nil{
			log.Println(err)
			//error handling not done
			return
		}
		usrs=append(usrs, usr)
		for _,usr :=range usrs {
			json_data,err :=json.Marshal(usr)
			if err!=nil{
				log.Println(err)
			}
			fmt.Println(string(json_data))
		}
	}
}